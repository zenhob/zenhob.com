---
title: "Zenhob.com"
subtitle: "Personal indie web nonsense"
layout: "layout/index.html"
---

Tune in to [Zenhob House Radio](https://radio.zenhob.com/stream.m3u)!

Co-work in the [Zenhob Cafe](https://beta.workfrom.coffee/cafes/zenhob)!

Video chat using [Jitsi Meet](https://meet.zenhob.com/)!

This site is built with [Eleventy](https://www.11ty.dev/) and deployed from
[Gitlab](https://gitlab.com/zenhob/zenhob.com/-/blob/main/www/index.md).

Created for some reason by [Zack Hobson](https://zackhobson.com/).


