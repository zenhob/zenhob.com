import React from 'react'
import ReactDOM from 'react-dom'

import Tuner from './Tuner'

ReactDOM.render(
  <Tuner src="https://radio.zenhob.com/radio/8000/radio.mp3" />,
  document.getElementById('tuner')
)

if (import.meta.hot) {
  import.meta.hot.accept()
}
