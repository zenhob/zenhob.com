resource "digitalocean_record" "radio" {
  domain = digitalocean_domain.zenhob.name
  type = "A"
  name = "radio"
  ttl = 300
  value = digitalocean_loadbalancer.radio.ip
}

resource "digitalocean_firewall" "radio" {
  name = "radio"
  droplet_ids = [ digitalocean_droplet.docker.id ]
  inbound_rule {
      protocol = "tcp"
      port_range = "8000"
      source_load_balancer_uids = [digitalocean_loadbalancer.radio.id]
      source_addresses = ["0.0.0.0/0", "::/0"]
  }
}

resource "digitalocean_certificate" "radio" {
  name    = "radio"
  type    = "lets_encrypt"
  domains = ["radio.zenhob.com"]
  lifecycle {
    create_before_destroy = true
  }
}

resource "digitalocean_loadbalancer" "radio" {
  name = "radio"
  region = "sfo3"

  forwarding_rule {
    entry_port = 443
    entry_protocol = "https"

    target_port = 8000
    target_protocol = "http"

    certificate_id = digitalocean_certificate.radio.id
  }

  forwarding_rule {
    entry_port = 8000
    entry_protocol = "http"

    target_port = 8000
    target_protocol = "http"
  }

  healthcheck {
    port = 8000
    protocol = "tcp"
  }

  droplet_ids = [digitalocean_droplet.docker.id]
}
