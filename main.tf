variable "do_token" {}
variable "ssh_pub_key" {}
variable "secret_key" {}
variable "access_id" {}
variable "vpn_source_addrs" {
  default = ["0.0.0.0/0", "::/0"]
}

provider "digitalocean" {
  token = var.do_token
  spaces_access_id = var.access_id
  spaces_secret_key = var.secret_key
}

resource "digitalocean_domain" "zenhob" {
  name = "zenhob.com"
}

resource "digitalocean_ssh_key" "hal" {
    name = "zack@hal"
    public_key = file(var.ssh_pub_key)
}

resource "digitalocean_firewall" "ssh" {
  name = "ssh"
  droplet_ids = [ digitalocean_droplet.docker.id ]
  inbound_rule {
      protocol = "tcp"
      port_range = "22"
      source_addresses = var.vpn_source_addrs
  }
  inbound_rule {
    protocol = "icmp"
  }

  outbound_rule {
    protocol = "tcp"
    port_range = "53"
  }
  outbound_rule {
    protocol = "udp"
    port_range = "53"
  }
  outbound_rule {
    protocol = "icmp"
  }
}


