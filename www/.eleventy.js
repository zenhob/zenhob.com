module.exports = function(eleventyConfig) {
  eleventyConfig.addPassthroughCopy("assets");
  eleventyConfig.addPassthroughCopy("_dist_");
  return {
    dir: {
        input: ".",
        includes: "_includes",
        output: "_public"
    }
  }
};
