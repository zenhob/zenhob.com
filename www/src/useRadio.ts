import { useEffect, useState } from 'react'

export enum RadioState {
  LOADING = 'loading',
  PLAYING = 'playing',
  PAUSED = 'paused'
}

const useRadio = (src : string) : [RadioState, HTMLAudioElement] => {
  const [radioState, setRadioState] = useState<RadioState>(RadioState.LOADING)
  const [audio, setAudioAPI] = useState<HTMLAudioElement>(new Audio())

  useEffect(() => {
    if (!src) { return }

    const api = new Audio(src)
    api.preload = "none"

    api.addEventListener('play', () => {
      setRadioState(RadioState.LOADING)
    })
    api.addEventListener('canplay', () => {
      setRadioState(RadioState.PLAYING)
    })
    api.addEventListener('pause', () => {
      api.src = src // clear the buffer so the live stream resumes correctly
      setRadioState(RadioState.PAUSED)
    })

    setRadioState(RadioState.PAUSED)
    setAudioAPI(api)
  }, [src])

  return [radioState, audio]
}

export default useRadio
