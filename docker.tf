resource "digitalocean_droplet" "docker" {
  image = "docker-18-04"
  name = "zenhob"
  region = "sfo3"
  size = "s-1vcpu-2gb"
  private_networking = false
  monitoring = true
  ssh_keys = [
    digitalocean_ssh_key.hal.fingerprint
  ]
}

resource "digitalocean_record" "docker" {
  domain = digitalocean_domain.zenhob.name
  type = "A"
  name = "docker"
  ttl = 300
  value = digitalocean_droplet.docker.ipv4_address
}

