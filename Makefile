export DOCKER_HOST=ssh://root@docker.zenhob.com 

default: show ps

provision: init apply meet_up radio_up

stop: radio_stop meet_stop

show:
	terraform show

www/node_modules:
	cd www && npm install

start: www/node_modules
	cd www && npm start

ps:
	docker ps

init: .terraform
	terraform init

apply: terraform.tfstate
	terraform apply

destroy:
	terraform destroy

radio_up: radio
	cd radio && docker-compose up -d

radio_stop: radio
	cd radio && docker-compose stop

meet_up: meet
	scp ./meet/hector-watermark.png root@docker.zenhob.com:/var
	cd meet && docker-compose up -d

meet_stop: meet
	cd meet && docker-compose stop

minecraft_up: minecraft
	cd minecraft && docker-compose up -d

minecraft_stop: minecraft
	cd minecraft && docker-compose stop

mc:
	docker exec -i minecraft_1 rcon-cli

