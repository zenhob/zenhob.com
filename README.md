# Zenhob.com

This is a set of scripts that will provision and run a docker host and some
apps on DigitalOcean, and a web site hosted on Gitlab pages.

## Setup

Prerequisites:

* `terraform` version `0.12.29` (or newer)
* `docker-compose` version `1.25.0` (or newer)

Edit `terraform.tfvars` to configure secrets:

```
do_token="XXX"
ssh_pub_key="/path/to/.ssh/id_rsa.pub"
secret_key="XXX"
access_id="XXX"
```

Initialize Terraform and provision the host and apps:

    make provision

To run the web site locally for dev and testing:

    make start

## Operation

The domains, storage and hosts are managed by Terraform.
View terraform and docker status:

    make

After changing Terraform config, review and apply the changes:

    make apply

Destroy the whole thing!

    make destroy

## Resources

### Web site

The domain `www.zenhob.com` is managed by terraform, and the web site is built
and re-deployed using Gitlab CI.

### Docker host

Accessible at `docker.zenhob.com`. This need to be provisioned with Terraform
before Jitsi Meet and Minecraft can be started.

### Radio

Accessible at `radio.zenhob.com:8000`.

Bring up the service:

    make radio_up

### Minecraft

Accessible at `minecraft.zenhob.com`.

The data is on a 10GB volume attached to the docker server as `/mnt/minecraft`,
available in the container as `/data`.

Bring up the service:

    make minecraft_up

Connect to the rcon console:

    make mc

### Jisti Meet

Accessible at `meet.zenhob.com`.

Bring up the service:

    make meet_up


