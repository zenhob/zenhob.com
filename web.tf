resource "digitalocean_record" "www" {
  domain = digitalocean_domain.zenhob.name
  type = "CNAME"
  name = "www"
  value = "zenhob.gitlab.io."
  ttl = 300
}

resource "digitalocean_record" "verification"  {
  domain = digitalocean_domain.zenhob.name
  type = "TXT"
  name = "_gitlab-pages-verification-code.www"
  value = "gitlab-pages-verification-code=5291d8bd5792fa837f1618bf56a140cc"
  ttl = 300
}
