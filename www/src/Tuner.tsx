import React, { useCallback } from 'react'

import useRadio, { RadioState } from './useRadio'
import style from './Tuner.module.css'

interface IconProps {
  radioState: RadioState
}
const Icon : React.FC<IconProps> = ({radioState}) => {
  switch (radioState) {
    case RadioState.PLAYING:
      return <i className={style.pause}></i>
    case RadioState.PAUSED:
      return <i className={style.play}></i>
    case RadioState.LOADING:
      return <i className={style.loading}></i>
  }
}

interface TunerProps {
  src: string
}
const Tuner : React.FC<TunerProps> = ({src}) => {
  const [radioState, audio] = useRadio(src)

  const togglePlaying = useCallback(async () => {
    try {
      switch(radioState) {
        case RadioState.PAUSED:
          await audio.play()
          break
        default:
          await audio.pause()
      }
    } catch (err) {
      console.log(err)
    }
  }, [radioState, audio])

  const handleKeyPress = useCallback(e => {
    e.preventDefault()
    switch (e.key) {
      case ' ':
      case 'Enter':
        togglePlaying()
      default:
    }
  }, [togglePlaying])

  return (
    <div tabIndex={0}
      className={style.playButton}
      onKeyPress={handleKeyPress}
      onClick={togglePlaying}
    >
      <Icon {...{radioState}} />
    </div>
  )
}

export default Tuner

