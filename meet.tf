
resource "digitalocean_record" "meet" {
  domain = digitalocean_domain.zenhob.name
  type = "A"
  name = "meet"
  ttl = 300
  value = digitalocean_droplet.docker.ipv4_address
}

resource "digitalocean_firewall" "jitsi" {
  name = "jitsi-meet"
  droplet_ids = [ digitalocean_droplet.docker.id ]
  inbound_rule {
      protocol = "tcp"
      port_range = "80"
      source_addresses = ["0.0.0.0/0", "::/0"]
  }
  inbound_rule {
      protocol = "tcp"
      port_range = "443"
      source_addresses = ["0.0.0.0/0", "::/0"]
  }
  inbound_rule {
      protocol = "tcp"
      port_range = "4443"
      source_addresses = ["0.0.0.0/0", "::/0"]
  }
  inbound_rule {
      protocol = "udp"
      port_range = "10000"
      source_addresses = ["0.0.0.0/0", "::/0"]
  }
  outbound_rule {
      protocol = "tcp"
      port_range = "1-65535"
      destination_addresses = ["0.0.0.0/0", "::/0"]
  }
  outbound_rule {
      protocol = "udp"
      port_range = "1-65535"
      destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}

